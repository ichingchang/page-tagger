
Unimelb Tagging Project.

For explanation of task, please refer to the To-Do-List_of_the_Tagging_Task.pdf


================================================================
We maintain a FAQs for tagging task here, and the content will be updated if necessary.


(1) How to run a python program:
    Run python program in terminal (command line, iTerm in Mac) by: python program_name.py. For example, python tag_helper.py


(2) If the contents of xxx.txt and xxx.html are different. Please tag based on xxx.txt


(3) Emails are the only case, where you may change the content (when the email is an irregular email). All the other content must be directly copied from xxx.txt files (or equivalent xxx.html files). 


(4) It is recommended to use modern text editor to view and edit json and .txt files. For example, Sublime Text, and Atom Editor are good choices. 


(5) Feel free to validate the tagged json, after you finish tagging each page.


(6) Leave comments in "comment" if you find it useful to do so. Let us know if you find something unusual, for example, if the contents of the xxx.html and xxx.txt are completely different.


(7) The tag_helper.py must be placed in the same folder as the xxx.html, xxx.tag.json files.


(8) Read comments at the beginning of programs, before running programs. The comments at the top of programs, contains information about how to run programs.


(9) If the main contents of a homepage are not in English, then we do not tag it. we mark the "is_personal_homepage" as F. Then skip this page.


(10) If there is no email on the webpage, leave the of value of email as an empty string, for example: "email": ""


(11) If a single publication is separated in two lines:
for example as in tagged_example/47, and tagged_example/21, our tag_helper.py program can deal with that.

    For example, if we want to tag publication using the tag_helper.py for tagged_example/47, the following input can be used:

    2 (choose mode 2)
    43 (start line of the first publication)
    127 (start line of the last publication)
    3 (step value, since there is a publication every 3 lines)
    3 (each publication occupies 3 lines)
    y (confirm)
    y (confirm overriding)


(12) If a publication is a dissertation/thesis supervised by the owner the webpages.
     we do not need to tag that publication, since dissertation/thesis is written by the student of the owner of webpage. This case does not happen very often. If this happen, the webpage will show that publication is a dissertation/thesis.


(13) It is highly recommended to use Anaconda Python environment, please install python 3.

    https://www.continuum.io/downloads


(14) Do not include title in name when tagging:

    The name of "Dr Andrew Ng", or "Prof Andrew Ng" should be "Andrew Ng".
    The name of "Melanie Kingsley, MD" should be "Melanie Kingsley", MD is a medical degree.
    However, if someone's name is  William Vann Rogers Jr, we want to include the Jr in the tagged name, since Jr is a generational designation.


(15) If the owner of the webpage has more than one form of name, for example, tagged_example/40.

    The owner of this page has two name forms, one with middle name, Khalil Gibran Muhammad, and one without, Khalil Muhammad.

    When tagging this kind of webpage, choose the most obvious name form on the page (name with largest font in this case), so that we will tag Khalil Muhammad as the name in this webpage.

    The url of this example is https://www.hks.harvard.edu/about/faculty-staff-directory/khalil-muhammad


(16) What if I can not run the python program provided, of there are errors when running.
     There are two possible situations: <1> You do not use the program correctly, read the instruction at the beginning of the program carefully  
     <2> There may be bug in the program, if you are sure about the bug, please let us know either by contacting the helper, or submit a issue in the 
     https://bitbucket.org/ichingchang/page-tagger/issues?status=new&status=open
     We welcome your input if you can debug the program.

     Or you may just checkout the latest version of the code by "git pull origin master", the bug may already be solved. The other solution is to checkout the issue tracker, to see if anyone else has already submit the issue (https://bitbucket.org/ichingchang/page-tagger/issues?status=new&status=open).


(17) Why the contents in the txt file are all the same line, under Windows system. 
    Because some text editor under windows system use different delimiter for new line. To avoid this, use modern text editor, such as Sublime Text.
    Please do not change the content of .txt .html files.


(18) What if the txt file is like this, how should we tag the publication, do we need to include the number in the publication.


12. Yu Sun, Nicholas Jing Yuan, Xing Xie, Kieran McDonald, Rui Zhang. Collaborative Intent Prediction with Real-Time Contextual Data, ACM Transactions on Information Systems (TOIS), accepted in January 2017.

13. Yu Gu, Guanli Liu, Jianzhong Qi, Hongfei Xu, Ge Yu, and Rui Zhang. The Moving K Diversified Nearest Neighbor Query. IEEE Transactions on Knowledge and Data Engineering (TKDE), 28(10): 2778-2792, 2016.


Answer: we do not include the number "12. ", "13. " in the publications.
You may want to use tag_helper.py to copy each line, and manually remove all the numbers at the beginning of the publications.

（19）About json file: if you got an error message saying it is not a valid json file, you can check your json file here: https://jsonformatter.curiousconcept.com/ to get some clues. 

（20） about  email again:
if there were more than one email addresses, choose the one belonging to the owner;
if there were more than one email addresses all belonging to the owner, choose the one existing in the txt file; if all existing in the txt, just choose one. 

