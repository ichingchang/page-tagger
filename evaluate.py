"""

Program to evaluate the predicted results.
predicted results will be sent to you
 when you finish tagging and validating.

If the folder structure is like this:

somepath/webpages/1/xxx.html
                   /xxx.tag.json
                   /xxx.txt

somepath/webpages/2/xxx.html
                   /xxx.tag.json
                   /xxx.txt

somepath/webpages/3/xxx.html
                   /xxx.tag.json
                   /xxx.txt
...

otherpath/predicted/1/xxx.predicted.json
otherpath/predicted/2/xxx.predicted.json
otherpath/predicted/3/xxx.predicted.json
...


The two path in user configuration should be:

GROUND_TRUTH_FOLDER = 'somepath/webpages/'
PREDICTION_FOLDER = 'otherpath/predicted/'

Run this program with:
python evaluate.py

Please use python 3, instead of python 2.
run "python --version" to see the version of python

environment setup:

pip install chardet

<option 1> pip install editdistance  (fastest if installed)
<option 2> pip install numpy (a little bit slow, if choose this way)
<option 3> <do not install those packages> (very slow, if choose this)


this program will create a folder called TEMP_FOLDER_NAME
the result of this program will be stored in TEMP_FOLDER_NAME

"""

import json
import sys
import os
from pprint import pprint
import chardet

# ###################################################
# ##################user configuration###############
# please change this according to to the path of your machine.
# GROUND_TRUTH_FOLDER should contain the ground truth you tagged
# PREDICTION_FOLDER should contain the predicted result we give you
GROUND_TRUTH_FOLDER = 'webpages/'
PREDICTION_FOLDER = '2/predicted/'

# For Windows user: when specifying the path, you need to separate
# each level by \\, since \ is a escape character in python
# for example, a folder, C:\Users\a\Desktop\folder_a
# when specifying the path of that folder in python source file,
# you need to enter C:\\Users\\a\\Desktop\\folder_a

# #################other configuration###############
# please do not change the other configurations
# this program will create a folder called TEMP_FOLDER_NAME
# the result of this program will be stored in TEMP_FOLDER_NAME
SIMILAR_PUBLICATION_THRESHOLD = 0.15
TEMP_FOLDER_NAME = 'tmp/'
DEBUG = False
EDITDISTANCE_MODE = 1
# ###################################################
# ###################################################


def levenshtein_slow(s1, s2):
    # code from
    # https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python

    if len(s1) < len(s2):
        return levenshtein_slow(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            # j+1 instead of j since previous_row and current_row are one
            # character longer
            deletions = current_row[j] + 1       # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]


def levenshtein(source, target):
    import numpy as np

    # code from
    # https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
    if len(source) < len(target):
        return levenshtein(target, source)

    # So now we have len(source) >= len(target).
    if len(target) == 0:
        return len(source)

    # We call tuple() to force strings to be used as sequences
    # ('c', 'a', 't', 's') - numpy uses them as values by default.
    source = np.array(tuple(source))
    target = np.array(tuple(target))

    # We use a dynamic programming algorithm, but with the
    # added optimization that we only need the last two rows
    # of the matrix.
    previous_row = np.arange(target.size + 1)
    for s in source:
        # Insertion (target grows longer than source):
        current_row = previous_row + 1

        # Substitution or matching:
        # Target and source items are aligned, and either
        # are different (cost of 1), or are the same (cost of 0).
        current_row[1:] = np.minimum(
            current_row[1:],
            np.add(previous_row[:-1], target != s))

        # Deletion (target grows shorter than source):
        current_row[1:] = np.minimum(
            current_row[1:],
            current_row[0:-1] + 1)

        previous_row = current_row

    return previous_row[-1]


def get_editdistance(string_a, string_b):

    global EDITDISTANCE_MODE

    if EDITDISTANCE_MODE == 1:
        try:
            # try to use the fastest implementation first
            import editdistance
            return editdistance.eval(string_a, string_b)

        except Exception as e:
            EDITDISTANCE_MODE = 2

    if EDITDISTANCE_MODE == 2:
        try:
            # the second fast solution
            return levenshtein(string_a, string_b)
        except Exception as e:
            EDITDISTANCE_MODE = 3

    if EDITDISTANCE_MODE == 3:
        # the slowest solution
        return levenshtein_slow(string_a, string_b)


def detect_encodings(filepath):
    """filepath must exists

    A detecting function with utf-8 first strategy.
    It will try to open the file with uft-8 encoding.
    If not utf-8, it will try to use chardet to detect
    encoding of txt file in the same folder.
    If fail, then it will detect encoding of the file itself.
    We use this strategy because the result given by chardet
    on Json file is not reliable.

    This function only works on this project, because there
    are some assumptions in this function:
    (1) folder structure, file with the same name.
    (2) txt file are almost all utf-8, because of generating process
    """

    def naive_detect():
        with open(filepath, 'rb') as fin:
            content = fin.read()
            results = chardet.detect(content)
        return results['encoding']

    try:
        with open(filepath, encoding='utf-8') as fin:
            fin.read()
        return 'utf-8'
    except Exception as e:
        pass

    try:
        if filepath[-4:] == '.txt':
            txt_file_path = filepath
        else:
            head, tail = os.path.split(filepath)
            filename = tail.split('.')[0]
            txt_file_path = os.path.join(head, filename + '.txt')
            return naive_detect(txt_file_path)
    except Exception as e:
        pass

    return naive_detect(filepath)


def get_json_file_path(folder, suffix='.tag.json'):
    '''get the path to the json file in the folder,
    the json file needs to have suffix "suffix" '''
    files = os.listdir(folder)

    filename = None
    ext = suffix
    length = len(ext)
    for f in files:
        if len(f) > length and f[-length:] == ext:
            filename = f[:-length]
            break

    if filename is None:
        print('\ncheck xxx.tag.json is present in folder' + folder)
        print('error, exiting...')
        exit(1)

    json_file_path = os.path.join(folder, filename + suffix)
    if not os.path.exists(json_file_path):
        print(json_file_path + ' does not exists, ERROR')
        exit(1)

    return json_file_path


def load_ground_truth_dict(folder_list):
    """output: a dictionary: filename --> publication list"""
    files = []
    for folder in folder_list:
        folder_path = os.path.join(GROUND_TRUTH_FOLDER, folder)
        json_file_path = get_json_file_path(folder_path)
        files.append(json_file_path)

    gt_filename_to_publication_text_list_dict = {}
    gt_filename_to_name_dict = {}
    gt_filename_to_email_dict = {}

    for f in files:
        filename = os.path.basename(f).split('.')[0]
        data = read_json(f)
        publications = data['publications']
        name = data['name']
        email = data['email']
        pub_text_list = []
        for pub_ele in publications:
            text = pub_ele['text']
            if text != 'COPY_SINGLE_PUBLICATION_HERE':
                pub_text_list.append(text)

        is_personal_homepage = data['is_personal_homepage']

        gt_filename_to_publication_text_list_dict[filename] = pub_text_list
        if is_personal_homepage == 'T':
            if name != 'COPY_OWNER_NAME_HERE':
                gt_filename_to_name_dict[filename] = name
            if email != 'COPY_AND_CORRECT_OWNER_EMAIL_HERE':
                gt_filename_to_email_dict[filename] = email

    return (gt_filename_to_publication_text_list_dict,
            gt_filename_to_name_dict,
            gt_filename_to_email_dict)


def load_prediction_dict(folder_list):
    """output: a dictionary: filename --> publication list"""
    files = []
    for folder in folder_list:
        folder_path = os.path.join(PREDICTION_FOLDER, folder)
        json_file_path = get_json_file_path(folder_path, '.predicted.json')
        files.append(json_file_path)

    pre_filename_to_publication_text_list_dict = {}
    pre_filename_to_name_dict = {}
    pre_filename_to_email_dict = {}

    for f in files:
        filename = os.path.basename(f).split('.')[0]
        data = read_json(f)

        if len(data) == 0:
            # empty dict means no predicted output for that web page
            continue

        name = data['name']

        email_list = data['email']
        if len(email_list) > 0:
            email = email_list[0]
        else:
            email = ''

        pub_text_list = []
        if 'publications' in data:
            publications = data['publications']
            for pub_ele in publications:
                text = pub_ele['text']
                pub_text_list.append(text)

        pre_filename_to_publication_text_list_dict[filename] = pub_text_list
        pre_filename_to_name_dict[filename] = name
        pre_filename_to_email_dict[filename] = email

    return (pre_filename_to_publication_text_list_dict,
            pre_filename_to_name_dict,
            pre_filename_to_email_dict)


def pub_can_be_found_in_list(publication, pub_list):
    if publication in pub_list:
        # exact match
        return True
    else:
        pub_to_ed_dict = {}
        min_ed = len(publication) * 100
        p_with_min_ed = None
        for p in pub_list:
            ed = get_editdistance(p, publication)
            pub_to_ed_dict[p] = ed
            if ed < min_ed:
                p_with_min_ed = p
                min_ed = ed

        min_ed_ratio = min_ed / float(len(publication))

        if DEBUG:
            print('--------------')
            print("publication", publication)
            print("p_with_min_ed", p_with_min_ed)
            print("min_ed", min_ed)
            print("min_ed_ratio", min_ed_ratio)
        if min_ed_ratio < SIMILAR_PUBLICATION_THRESHOLD:
            return True
        else:
            return False


def calculate_f_score_publication(gt_dict, prediction_dict):
    """input two dictionary: filename to publication list
    output, precision, recall, and f-score"""

    # precision
    total_num_predicted_pubs = 0
    num_corrected_predicted_pubs = 0
    for filename in prediction_dict:
        predicted_pubs = prediction_dict[filename]
        total_num_predicted_pubs += len(predicted_pubs)
        if filename in gt_dict:
            gt_pubs = gt_dict[filename]
            for pub in predicted_pubs:
                if pub_can_be_found_in_list(pub, gt_pubs):
                    num_corrected_predicted_pubs += 1

    precision = num_corrected_predicted_pubs / float(total_num_predicted_pubs)

    # recall
    total_num_gt_pubs = 0
    num_corrected_predicted_gt_pubs = 0
    for filename in gt_dict:
        gt_pubs = gt_dict[filename]
        total_num_gt_pubs += len(gt_pubs)
        if filename in prediction_dict:
            prediction_pubs = prediction_dict[filename]
            for pub in gt_pubs:
                if pub_can_be_found_in_list(pub, prediction_pubs):
                    num_corrected_predicted_gt_pubs += 1

    recall = num_corrected_predicted_gt_pubs / float(total_num_gt_pubs)

    if num_corrected_predicted_pubs != num_corrected_predicted_gt_pubs:
        message = """Warning! num_corrected_predicted_pubs !=
         num_corrected_predicted_gt_pubs\n"""
        print(message)
        log_path = os.path.join(TEMP_FOLDER_NAME, 'log.txt')
        with open(log_path, 'a', encoding='utf-8') as fout:
            fout.write(message)

    f_score = (2 * precision * recall) / (precision + recall)

    return (precision, recall, f_score)


def calculate_f_score(gt_dict, prediction_dict):
    """input two dictionary: filename to name/email
    output, precision, recall, and f-score"""

    # precision
    total_num_predicted = 0
    num_corrected_predicted = 0
    for filename in prediction_dict:
        predicted_content = prediction_dict[filename]
        if predicted_content != '':
            total_num_predicted += 1
            if filename in gt_dict:
                gt_content = gt_dict[filename]
                if predicted_content == gt_content:
                    num_corrected_predicted += 1

    precision = num_corrected_predicted / float(total_num_predicted)

    # recall
    total_num_gt = 0
    num_corrected_predicted_gt = 0
    for filename in gt_dict:
        gt_content = gt_dict[filename]
        if gt_content != '':
            total_num_gt += 1
            if filename in prediction_dict:
                predicted_content = prediction_dict[filename]
                if gt_content == predicted_content:
                    num_corrected_predicted_gt += 1

    recall = num_corrected_predicted_gt / float(total_num_gt)

    if num_corrected_predicted != num_corrected_predicted_gt:
        message = """Warning! num_corrected_predicted !=
         num_corrected_predicted_gt\n"""
        print(message)
        log_path = os.path.join(TEMP_FOLDER_NAME, 'log.txt')
        with open(log_path, 'a', encoding='utf-8') as fout:
            fout.write(message)

    # f score
    f_score = (2 * precision * recall) / (precision + recall)

    return (precision, recall, f_score)


def get_folder_list():
    print('Please enter the starting folder number for evaluating.')
    answer = input()
    try:
        start_folder_num = int(answer)
    except Exception as e:
        print('wrong input, exiting...')

    print('Please enter the end folder number for evaluating (inclusive)')
    answer = input()
    try:
        end_folder_num = int(answer)
    except Exception as e:
        print('wrong input, exiting...')
        exit(1)

    folder_num_lists = []
    for i in range(start_folder_num, end_folder_num + 1):
        folder_num_lists.append(i)

    print('print the folder numbers for validation:')
    print(folder_num_lists)
    print('is it correct? ([y]/n)')

    answer = input()
    if answer.lower() == 'n':
        print('user choose to exit...')
        exit()

    for i in range(len(folder_num_lists)):
        folder_num_lists[i] = str(folder_num_lists[i])

    for folder in folder_num_lists:
        gt_folder_path = os.path.join(GROUND_TRUTH_FOLDER, folder)
        prediction_folder_path = os.path.join(PREDICTION_FOLDER, folder)
        if not os.path.exists(gt_folder_path):
            print(gt_folder_path, 'does not exists, exiting...')
            exit(1)
        if not os.path.exists(prediction_folder_path):
            print(prediction_folder_path, 'does not exists, exiting...')
            exit(1)

    return folder_num_lists


def save_as_json(content, json_path):
    with open(json_path, 'w', encoding='utf-8') as fout:
        json.dump(content, fout, indent=2, sort_keys=True)


def read_json(json_path):
    with open(json_path, encoding=detect_encodings(json_path)) as fin:
        data = json.load(fin)
    return data


def generate_gt_tmp_files(folder_list):

    f_to_pubs, f_to_name, f_to_email = load_ground_truth_dict(folder_list)

    pubs_path = os.path.join(TEMP_FOLDER_NAME, 'gt_publications.json')
    save_as_json(f_to_pubs, pubs_path)

    name_path = os.path.join(TEMP_FOLDER_NAME, 'gt_name.json')
    save_as_json(f_to_name, name_path)

    email_path = os.path.join(TEMP_FOLDER_NAME, 'gt_email.json')
    save_as_json(f_to_email, email_path)


def generate_prediction_tmp_files(folder_list):

    f_to_pubs, f_to_name, f_to_email = load_prediction_dict(folder_list)

    pubs_path = os.path.join(TEMP_FOLDER_NAME, 'predicted_publications.json')
    save_as_json(f_to_pubs, pubs_path)

    name_path = os.path.join(TEMP_FOLDER_NAME, 'predicted_name.json')
    save_as_json(f_to_name, name_path)

    email_path = os.path.join(TEMP_FOLDER_NAME, 'predicted_email.json')
    save_as_json(f_to_email, email_path)


def evaluate():
    gt_pubs_dict = read_json(os.path.join(
        TEMP_FOLDER_NAME, 'gt_publications.json'))
    gt_name_dict = read_json(os.path.join(
        TEMP_FOLDER_NAME, 'gt_name.json'))
    gt_email_dict = read_json(os.path.join(
        TEMP_FOLDER_NAME, 'gt_email.json'))

    predicted_pubs_dict = read_json(os.path.join(
        TEMP_FOLDER_NAME, 'predicted_publications.json'))
    predicted_name_dict = read_json(os.path.join(
        TEMP_FOLDER_NAME, 'predicted_name.json'))
    predicted_email_dict = read_json(os.path.join(
        TEMP_FOLDER_NAME, 'predicted_email.json'))

    result_dict = {}
    pub_result_dict = {}
    name_result_dict = {}
    email_result_dict = {}

    precision, recall, f_score = calculate_f_score_publication(
        gt_pubs_dict, predicted_pubs_dict)
    pub_result_dict['precision'] = precision
    pub_result_dict['recall'] = recall
    pub_result_dict['f_score'] = f_score
    result_dict['publication_prediction'] = pub_result_dict

    precision, recall, f_score = calculate_f_score(
        gt_name_dict, predicted_name_dict)
    name_result_dict['precision'] = precision
    name_result_dict['recall'] = recall
    name_result_dict['f_score'] = f_score
    result_dict['name_prediction'] = name_result_dict

    precision, recall, f_score = calculate_f_score(
        gt_email_dict, predicted_email_dict)
    email_result_dict['precision'] = precision
    email_result_dict['recall'] = recall
    email_result_dict['f_score'] = f_score
    result_dict['email_prediction'] = email_result_dict

    print('\nevaluation result:\n')
    pprint(result_dict)

    result_path = os.path.join(TEMP_FOLDER_NAME, 'evaluation_result.json')
    save_as_json(result_dict, result_path)
    print('\nresult save to ' + result_path)
    print('Done!')


def main():
    PY3 = sys.version_info[0] == 3
    if not PY3:
        print('please use python 3 to run the program, exiting...')
        exit(1)

    folder_list = get_folder_list()

    if not os.path.exists(TEMP_FOLDER_NAME):
        os.mkdir(TEMP_FOLDER_NAME)

    generate_gt_tmp_files(folder_list)

    generate_prediction_tmp_files(folder_list)

    evaluate()


def test():
    a = "I love unim4elb"
    b = "Am I love unimelb?"
    c = get_editdistance(a, b)
    print(c)


if __name__ == '__main__':
    main()
    # test()
